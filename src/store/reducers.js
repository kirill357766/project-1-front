import { combineReducers } from "redux";
import { projectsListReducer } from "./projectsList/reducer";

export const rooteReducer = combineReducers({
  projectsList: projectsListReducer,
});
