function createData(
  id,
  projectName,
  customer,
  functionalDirection,
  subjectArea,
  requestAuthor,
  cardStatus,
  completionDate
) {
  return {
    id,
    projectName,
    customer,
    functionalDirection,
    subjectArea,
    requestAuthor,
    cardStatus,
    completionDate,
  };
}

const initialState = {
  projectsList: [
    createData(
      "1",
      "Цунтрализация ИТ-инфраструктуры",
      "PONY EXPRESS",
      "ЦЕНТРАЛИЗАЦИЯ",
      "ТРАНСПОРТ И ЛОГИСТИКА",
      "ИВАНОВА И. И.",
      "Активная",
      "12-29-2022"
    ),
    createData(
      "2",
      "Rb ИТ-инфраструктуры",
      "PONY EXPRESS",
      "ЦЕНТРАЛИЗАЦИЯ",
      "ТРАНСПОРТ И ЛОГИСТИКА",
      "ИВАНОВА И. И.",
      "В Архиве",
      "12-28-2021"
    ),
    createData(
      "3",
      "ИТ-инфраструктуры",
      "PONY EXPRESS",
      "ЦЕНТРАЛИЗАЦИЯ",
      "ТРАНСПОРТ И ЛОГИСТИКА",
      "СИДОРОВА И. И.",
      "Активная",
      "07-27-2022"
    ),
    createData(
      "4",
      "Фиксация ИТ-инфраструктуры",
      "PONY EXPRESS",
      "ЦЕНТРАЛИЗАЦИЯ",
      "ТРАНСПОРТ И ЛОГИСТИКА",
      "ИВАНОВА И. И.",
      "Активная",
      "11-26-2022"
    ),
    createData(
      "5",
      "Цaнтрализация ИТ-инфраструктуры",
      "EXPRESS PONY",
      "ЦЕНТРАЛИЗАЦИЯ",
      "ТРАНСПОРТ И ЛОГИСТИКА",
      "ИВАНОВА И. И.",
      "Активная",
      "10-25-2022"
    ),
    createData(
      "6",
      "Цaнтрализация ФИТ-инфраструктуры",
      "EXPRESS PONY",
      "ЦЕНТРАЛИЗАЦИЯ",
      "ЛОГИСТИКА",
      "ИВАНОВА И. И.",
      "Активная",
      "10-25-2023"
    ),
  ],
  searchText: "",
  searchOption: 0,
};

export const projectsListReducer = (state = initialState, action) => {
  switch (action.type) {
    case "SET_SEARCHFIELD_TEXT":
      return { ...state, searchText: action.payload };
    case "SET_SEARCH_OPTION":
      return { ...state, searchOption: action.payload };
    default:
      return state;
  }
};
