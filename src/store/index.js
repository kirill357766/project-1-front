import { applyMiddleware, createStore } from "redux";
import reduxThunk from "redux-thunk";
import { rooteReducer } from "./reducers";

export const STORE = createStore(rooteReducer, applyMiddleware(reduxThunk));
