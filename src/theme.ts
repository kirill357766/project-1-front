import { createTheme, responsiveFontSizes } from "@mui/material/styles";
import { ruRU } from "@mui/material/locale";

let theme = createTheme(
  {
    palette: {
      primary: {
        main: "#BEEBFF",
      },
      //secondary: purple,

      secondary: {
        main: "#ffff",
        //main: '#673ab7',
        // #4ba0d7
      },
    },
    shape: {
      borderRadius: 8,
    },
    typography: {
      fontFamily: ["Roboto", "sans-serif"].join(","),
    },
    components: {
      MuiButton: {
        variants: [
          {
            props: { variant: "contained", color: "secondary" },
            style: {
              color: "black",
              height: "40px",
              filter: "drop-shadow(0px 0px 3px rgba(0, 0, 0, 0.25))",
              textTransform: "none",
              "&:hover": {
                backgroundColor: "#ffff",
              },
            },
          },
        ],
      },
    },
  },
  ruRU
);

theme = responsiveFontSizes(theme);
export default theme;
