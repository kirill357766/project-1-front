import ThemeProvider from "@mui/material/styles/ThemeProvider";
import React from "react";
import PrimarySearchAppBar from "./components/PrimarySearchAppBar";
import theme from "./theme";
import MainProjectsList from "./components/MainProjectsList";
import { Provider } from "react-redux";
import { STORE } from "./store";

const App = () => {
  return (
    <div>
      <Provider store={STORE}>
        <ThemeProvider theme={theme}>
          <PrimarySearchAppBar />
          <MainProjectsList />
        </ThemeProvider>
      </Provider>
    </div>
  );
};

export default App;
