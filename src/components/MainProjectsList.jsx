import * as React from "react";
import Box from "@mui/material/Box";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import TableSortLabel from "@mui/material/TableSortLabel";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Paper from "@mui/material/Paper";
import { visuallyHidden } from "@mui/utils";
import { useSelector } from "react-redux";

// TODO: Переписать в tsx, когда будет известен формат данных
// TODO: Сделать автокомплит, вместо поиска
// TODO: Eсли в опциях ничего не выбрано, сделать показ всех карточек

function descendingComparator(a, b, orderBy) {
  // Сортировка по дате
  if (Date.parse(a[orderBy]) && Date.parse(b[orderBy])) {
    if (Date.parse(b[orderBy]) < Date.parse(a[orderBy])) {
      return -1;
    }
    if (Date.parse(b[orderBy]) > Date.parse(a[orderBy])) {
      return 1;
    }
    return 0;
  }

  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

const headCells = [
  {
    id: "projectName",
    disablePadding: true,
    label: "Наименование проекта",
  },
  {
    id: "customer",
    disablePadding: false,
    label: "Заказчик",
  },
  {
    id: "functionalDirection",
    disablePadding: false,
    label: "Функциональное направление",
  },
  {
    id: "subjectArea",
    disablePadding: false,
    label: "Предметная область",
  },
  {
    id: "requestAuthor",
    disablePadding: false,
    label: "Автор заявки",
  },
  {
    id: "cardStatus",
    disablePadding: false,
    label: "Статус карточки",
  },
  {
    id: "completionDate",
    disablePadding: false,
    label: "Срок завершения проекта",
  },
];

function EnhancedTableHead(props) {
  const { order, orderBy, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align="left"
            padding={headCell.disablePadding ? "none" : "normal"}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <Box component="span" sx={visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </Box>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

const EnhancedTableToolbar = () => {
  return (
    <Toolbar
      sx={{
        pl: { sm: 1 },
        pr: { xs: 1, sm: 1 },
      }}
    >
      <Typography
        sx={{ flex: "1 1 100%" }}
        variant="h6"
        id="tableTitle"
        component="div"
      >
        Реестр карточек
      </Typography>
    </Toolbar>
  );
};

const nameSearchQuals = [
  "projectName",
  "customer",
  "functionalDirection",
  "subjectArea",
  "requestAuthor",
  "cardStatus",
  "completionDate",
];

const MainProjectsList = () => {
  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("project-name");
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const state = useSelector((state) => state.projectsList);

  const initialRows = state.projectsList;

  const rowsToFilter = initialRows.map((row) => {
    const choosedRow = nameSearchQuals[state.searchOption];
    return {
      id: row.id,
      choosedRow: row[choosedRow] && row[choosedRow].toLowerCase(),
    };
  });

  const newRows = rowsToFilter.filter((row) => {
    if (!row.choosedRow) return false;
    return row.choosedRow.includes(state.searchText.toLowerCase());
  });

  const newRowsIds = newRows.map((row) => {
    return row.id;
  });

  const rows = initialRows.filter((row) => {
    return newRowsIds.includes(row.id);
  });

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  return (
    <Box sx={{ width: "100%", mt: 5 }}>
      <Paper sx={{ width: "100%", mb: 2, pl: 1 }}>
        <EnhancedTableToolbar />
        <TableContainer>
          <Table
            sx={{ minWidth: 750 }}
            aria-labelledby="tableTitle"
            size="medium"
          >
            <EnhancedTableHead
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
            />
            <TableBody>
              {rows
                .slice()
                .sort(getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <React.Fragment key={row.id}>
                      <TableRow hover tabIndex={-1}>
                        <TableCell
                          component="th"
                          id={labelId}
                          scope="row"
                          padding="none"
                        >
                          {row.projectName}
                        </TableCell>
                        <TableCell align="left">{row.customer}</TableCell>
                        <TableCell align="left">
                          {row.functionalDirection}
                        </TableCell>
                        <TableCell align="left">{row.subjectArea}</TableCell>
                        <TableCell align="left">{row.requestAuthor}</TableCell>
                        <TableCell align="left">
                          <Typography
                            sx={{
                              maxWidth: 100,
                              textAlign: "center",
                              borderRadius: 2,
                              backgroundColor:
                                (row.cardStatus === "Активная" && "green") ||
                                (row.cardStatus === "В Архиве" && "orange"),
                            }}
                          >
                            {row.cardStatus}
                          </Typography>
                        </TableCell>
                        <TableCell align="left">{row.completionDate}</TableCell>
                      </TableRow>
                      <TableRow></TableRow>
                    </React.Fragment>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow
                  style={{
                    height: 53 * emptyRows,
                  }}
                >
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </Box>
  );
};

export default MainProjectsList;
