import React from "react";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import { useSelector, useDispatch } from "react-redux";

const AutocompleteSearchInput = (props) => {
  const state = useSelector((state) => state.projectsList);
  const dispatch = useDispatch();

  const nameSearchQuals = [
    "projectName",
    "customer",
    "functionalDirection",
    "subjectArea",
    "requestAuthor",
    "cardStatus",
    "completionDate",
  ];

  const initialRows = state.projectsList;

  const rowsToFilter = initialRows.map((row) => {
    const choosedRow = nameSearchQuals[state.searchOption];
    return row[choosedRow];
  });

  const rowsToFilterSet = new Set(rowsToFilter);

  const searchInputOptions = [];

  rowsToFilterSet.forEach((option) => {
    searchInputOptions.push({ label: option });
  });

  return (
    <Autocomplete
      disablePortal
      id="autocomplete-search-input"
      placeholder="Search…"
      value={state.searchText}
      isOptionEqualToValue={(option, value) => option.label === value.label}
      onChange={(event, value) => {
        dispatch({
          type: "SET_SEARCHFIELD_TEXT",
          payload: value ? (value.label ? value.label : "") : "",
        });
      }}
      onInputChange={(event) => {
        dispatch({
          type: "SET_SEARCHFIELD_TEXT",
          payload: event ? (event.target ? event.target.value : "") : "",
        });
      }}
      freeSolo
      options={searchInputOptions}
      renderInput={(params) => <TextField {...params} label="Search..." />}
    />
  );
};

export default AutocompleteSearchInput;
