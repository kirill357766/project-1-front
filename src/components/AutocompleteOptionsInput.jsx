import * as React from "react";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import { useDispatch, useSelector } from "react-redux";

// TODO: сделать enum для searchInputOptions

const AutocompleteOptionsInput = () => {
  const state = useSelector((state) => state.projectsList);
  const dispatch = useDispatch();
  const searchInputOptions = [
    { label: "Наименование проекта", id: "0" },
    { label: "Заказчик", id: "1" },
    { label: "Функциональное направление", id: "2" },
    { label: "Предметная область", id: "3" },
    { label: "Автор заявки", id: "4" },
    { label: "Статус карточки", id: "5" },
    { label: "Срок завершения проекта", id: "6" },
  ];

  return (
    <Autocomplete
      disablePortal
      id="search-input-options"
      options={searchInputOptions}
      value={searchInputOptions[state.searchOption]}
      isOptionEqualToValue={(option, value) => option.id === value.id}
      size="small"
      onChange={(event, value) => {
        dispatch({
          type: "SET_SEARCH_OPTION",
          payload: value ? value.id : state.searchOption,
        });
        dispatch({
          type: "SET_SEARCHFIELD_TEXT",
          payload: "",
        });
      }}
      sx={{ width: 300 }}
      renderInput={(params) => <TextField {...params} label="Поиск по" />}
    />
  );
};

export default AutocompleteOptionsInput;
